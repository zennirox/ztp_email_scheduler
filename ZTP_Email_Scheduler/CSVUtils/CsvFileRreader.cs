﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Infrastructure;
using log4net;
using Logger;

namespace CSVUtils
{
    public class CsvReaderWrapper<T> :
        IFileReader<T> where T : class, new()
    {
        private static readonly ILog _log = LoggerHelper.GetLogger();

        private readonly CsvReader _csvReader;
        private int _lastReadRecord;

        public CsvReaderWrapper(string path)
        {
            _csvReader = new CsvReader(File.OpenText(path));
            if (string.IsNullOrEmpty(path) || !File.Exists(path))
            {
                throw new ArgumentException($"{nameof(path)} is not valid");
            }
        }
        public T[] ReadRecords(int maxRecordsToRead)
        {
            if (maxRecordsToRead < 0)
            {
                throw new ArgumentOutOfRangeException(
                    $"{nameof(maxRecordsToRead)} is " +
                    $"{maxRecordsToRead} when min value is 0"
                );
            }

            lock (_lock)
            {
                try
                {
                    var records = _csvReader.GetRecords<T>()
                        .Skip(_lastReadRecord)
                        .Take(maxRecordsToRead)
                        .ToArray();

                    _log.Info($" {_lastReadRecord} records read.");
                    _lastReadRecord = records.Length;
                    return records;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Reading csv failed", ex);
                }
            }
        }

        private readonly object _lock = new object();
        public bool TryReadRecords(int maxRecordsToRead, out T[] records)
        {
            records = default(T[]);

            if (maxRecordsToRead < 0)
            {
                throw new ArgumentOutOfRangeException(
                    $"{nameof(maxRecordsToRead)} is " +
                    $"{maxRecordsToRead} when min value is 0"
                );
            }

            lock (_lock)
            {
                try
                {
                    records = _csvReader.GetRecords<T>()
                        .Skip(_lastReadRecord)
                        .Take(maxRecordsToRead)
                        .ToArray();

                    _lastReadRecord = records.Length;
                    _log.Info($" {_lastReadRecord} records read.");
                    return true;

                }
                catch (Exception e)
                {
                    _log.Error(e);
                    return false;
                }
            }
        }
        public Task<T[]> ReadRecordsAsync(int maxRecordsToRead)
        {
            throw new NotImplementedException();
        }

        public T[] ReadRecords()
        {
            return _csvReader.EnumerateRecords(new T()).ToArray();
        }
    }
}
