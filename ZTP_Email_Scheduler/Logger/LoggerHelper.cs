﻿using System.Runtime.CompilerServices;
using log4net;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Logger
{
    public class LoggerHelper
    {
        public static ILog GetLogger(
            [CallerFilePath] string fileName = ""
        )
        {
            return LogManager.GetLogger(fileName);
        }
    }
}
