﻿using System;
using Infrastructure;
using Quartz;
using Quartz.Impl;

namespace QuartzNet
{
    public class SchedulerCreator<T>: ISchedulerWorker where T : IJob
    {
        private readonly IScheduler _scheduler;
        public SchedulerCreator(IApplication application)
        {
            if (application is IJob job)
            {
                _scheduler = new StdSchedulerFactory().GetScheduler().Result;
                _scheduler.JobFactory = new JobFactory(job);
            }
            else
            {
                throw new Exception(
                    $"{nameof(application)} is not a {typeof(T)} type"
                );
            }
        }
        public async void Start(int jobInterval) 
        {
            try
            {
                //Logger np do Nloga
                //LogProvider.SetCurrentLogProvider(new ConsoleLogProvider());
                var job = JobBuilder.Create<T>().Build();
                var trigger = TriggerBuilder.Create()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(jobInterval)
                        .RepeatForever())
                    .Build();

                await _scheduler.ScheduleJob(job, trigger);
                await _scheduler.Start();
            }
            catch (SchedulerException se)
            {
                Console.WriteLine(se);
            }
        }

        public void Stop()
        {
            _scheduler.Shutdown();
        }
    }
}
