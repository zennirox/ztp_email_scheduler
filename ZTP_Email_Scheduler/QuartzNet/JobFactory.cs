﻿using Infrastructure;
using Quartz;
using Quartz.Spi;

namespace QuartzNet
{
    public class JobFactory : IJobFactory
    {
        private readonly IJob _application;

        public JobFactory(IJob application)
        {
            _application = application;
        }
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _application;
        }

        public void ReturnJob(IJob job)
        {
           // throw new NotImplementedException();
        }
    }
}
