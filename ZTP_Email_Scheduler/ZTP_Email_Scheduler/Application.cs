﻿using System;
using System.Threading.Tasks;
using Infrastructure;
using log4net;
using Logger;
using Quartz;

namespace ZTPEmailScheduler
{
    public class Application : IApplication, IJob
    {
        private static readonly ILog _log = LoggerHelper.GetLogger();
        private readonly IFileReader<Record> _csvReader;
        private readonly IMessageSender<Record> _mailerSender;

        public Application(
            IFileReader<Record> csvReader,
            IMessageSender<Record> mailerSender
        )
        {
            _csvReader = csvReader
                ?? throw new ArgumentNullException(nameof(csvReader));
            _mailerSender = mailerSender
                ?? throw new ArgumentNullException(nameof(mailerSender));
        }
       
        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                var records = _csvReader.ReadRecords(100);
                foreach (var record in records)
                {
                    if (!record.IsDiscountValid())
                        continue;
                    _log.Info("JA GUT DONE");
                    _mailerSender.TrySendMessage(record);
                }
            }
            catch (Exception ex)
            {
                _log.Info(ex);
            }

            return Task.CompletedTask;
        }
    }
}
