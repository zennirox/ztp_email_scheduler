﻿using System.Configuration;
using Autofac;
using CSVUtils;
using FluentMailerUtils;
using Infrastructure;
using log4net;
using Logger;
using QuartzNet;

namespace ZTPEmailScheduler
{
    public class ConfigureContainer
    {
        private static readonly ILog _log = LoggerHelper.GetLogger();

        public IContainer Configure(ContainerBuilder containerBuilder)
        {
            var pathToCsvFile = ConfigurationManager.AppSettings["csvFilePath"];
            _log.Info($"CSV File path form AppSettings: {pathToCsvFile}");
            var csvReaderWrapper = new CsvReaderWrapper<Record>(pathToCsvFile);

            containerBuilder
                .RegisterInstance(csvReaderWrapper)
                .As<IFileReader<Record>>();

            var email = ConfigurationManager.AppSettings["email"];
            _log.Info($"From AppSettings: {email}");
            var password = ConfigurationManager.AppSettings["password"];
            var smtp = ConfigurationManager.AppSettings["smtp"];
            _log.Info($"From AppSettings: {smtp}");
            var port = ConfigurationManager.AppSettings["port"];
            _log.Info($"From AppSettings: {port}");


            var fluentMailer = new FluentMailerSender(
                email,
                password, 
                smtp,
                int.Parse(port)
                );

            containerBuilder
                .RegisterInstance(fluentMailer)
                .As<IMessageSender<Record>>();

            containerBuilder
                .RegisterType<Application>()
                .As<IApplication>();
            containerBuilder
                .RegisterType<SchedulerCreator<Application>>()
                .As<ISchedulerWorker>();

            return containerBuilder.Build();
        }
    }
}
