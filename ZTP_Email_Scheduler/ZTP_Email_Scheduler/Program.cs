﻿using System;
using System.Threading;
using Autofac;
using Infrastructure;
using log4net;
using Logger;
using Topshelf;


namespace ZTPEmailScheduler
{
    internal class Program
    {
        private static readonly ILog _log = LoggerHelper.GetLogger();
        public Program()
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            {
                _log.Error(e);
            };
        }
        private static void Main(string[] args)
        {
            IContainer container;
            try
            {
                container = new ConfigureContainer()
                    .Configure(new ContainerBuilder()
                    );
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return;
            }

            using (container.BeginLifetimeScope())
            {
                var schedulerWorker = container.Resolve<ISchedulerWorker>();
                var topShelfHost = HostFactory.Run(x =>
                {
                    x.Service<ISchedulerWorker>(s =>
                    {
                        s.ConstructUsing(name => schedulerWorker);
                        s.WhenStarted(sch => sch.Start(1));
                        s.WhenStopped(sch => sch.Stop());
                    });
                    x.RunAsLocalSystem();
                    x.SetDescription("ZTPScheduler Host");
                    x.SetDisplayName("ZTPScheduler Worker");
                    x.SetServiceName("ZTPScheduler Worker");
                    x.OnException(ex =>
                    {
                        _log.Error(ex);
                    });
                    x.StartAutomatically();
                });

                var exitCode = (int) Convert.ChangeType(topShelfHost, topShelfHost.GetTypeCode());
                Environment.ExitCode = exitCode;
                Thread.Sleep(-1);
            }
        }
    }
}
