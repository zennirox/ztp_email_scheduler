﻿using System;

namespace CsvHelper
{
    public class Record
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Adress { get; set; }
        public float Discount { get; set; }
        public DateTime DiscountExpire { get; set; }
    }
}
