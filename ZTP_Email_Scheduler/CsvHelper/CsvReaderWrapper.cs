﻿using System;
using System.IO;
using System.Linq;

namespace CsvHelper
{
    public class CsvReaderWrapper : ICsvReaderWrapper
    {
        public Record[] ReadRecords(int recordsToRead, string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException(nameof(path));
            }

            var txtReader = File.OpenText(path);
            var csv = new CsvReader(txtReader);
            var csvRecords = csv.EnumerateRecords(new Record());

            return csvRecords.ToArray();
            return null;
        }
    }
}
