﻿namespace Infrastructure
{
    public interface IMessageSender<T>
    {
        bool TrySendMessage(T record);
    }
}
