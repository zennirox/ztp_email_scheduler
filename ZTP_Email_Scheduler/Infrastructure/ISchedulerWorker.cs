﻿namespace Infrastructure
{
    public interface ISchedulerWorker
    {
        void Start(int jobInterval);
        void Stop();
    }
}
