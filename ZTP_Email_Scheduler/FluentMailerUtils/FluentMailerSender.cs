﻿using System;
using System.Net;
using System.Net.Mail;
using FluentEmail.Core;
using FluentEmail.Smtp;
using Infrastructure;
using log4net;
using Logger;

namespace FluentMailerUtils
{
    public class FluentMailerSender : IMessageSender<Record>
    {
        private static readonly ILog _log = LoggerHelper.GetLogger();
        private readonly SmtpSender _smtpSender;
        private readonly string _mail;

        public FluentMailerSender(
            string mail,
            string pass,
            string smtpAddr,
            int port
            )
        {
            if (string.IsNullOrEmpty(mail))
                throw new ArgumentException(nameof(mail));
            if (string.IsNullOrEmpty(pass))
                throw new ArgumentException(nameof(pass));
            if (string.IsNullOrEmpty(smtpAddr))
                throw new ArgumentException(nameof(smtpAddr));

            _smtpSender = new SmtpSender(
                new SmtpClient(smtpAddr, port)
                {
                    UseDefaultCredentials = false,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(mail, pass)
                }
            );
            _mail = mail;
        }
        public bool TrySendMessage(Record record)
        {
            try
            {
                _log.Info($"Creating email for {record.Address}");
                var email = Email.From(_mail)
                    .To(record.Address)
                    .Subject("Testy FluentEmaila")
                    .Body(
                        $@"Hello {record.Name} {record.Surname}. 
Your discount {record.Discount}% will 
expire at {record.DiscountExpire}"
                    );

                _log.Info($"Begin sending log to: {record.Address}");
                _smtpSender.Send(email);
                _log.Info($"Mail sent to: {record.Address}");
                return true;
            }
            catch (Exception e)
            {
                _log.Error(e);
                return false;
            }
        }
    }
}
